require 'json'

class Repo
  attr_reader :client, :json

  def initialize(repo_name = nil, json = nil)
    @client = Client.new
    @json = json || client.get("https://api.github.com/users/#{repo_name}")
  end

  def self.from_repo_name(username)
    new(username)
  end

  def self.from_json(json)
    new(nil, json)
  end

  def assignees
    resp = client.get(json['assignees_url'].gsub('{/user}', ''))

    resp.map do |json|
      User.from_json(json)
    end
  end
end
