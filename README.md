## 1. Setup a project

Please install 2 gems

- gem install http
- gem install json
- set ENV['TOKEN'] into api_navigator.rb

## 2. Run the code
- ruby api_navigator.rb

## 3. Run through ERB
```sh
require './github/user.rb'
require './github/client.rb'
require './github/repo.rb'
require './github/organization.rb'

user = User.from_username 'example' 
user.repos 
# returns an array of Repo instances 
user.followers[0].organizations 
# returns an array of Organization instances 
user.repos[0].assignees 
# returns an array of User instances 
# etc. 
```

## More information
- For create a personal access token [Click here](https://docs.github.com/en/github/authenticating-to-github/keeping-your-account-and-data-secure/creating-a-personal-access-token)
- curl request

```
curl -H "Authorization: token ENV['TOKEN']" https://api.github.com/users/user_name
```