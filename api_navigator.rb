require './github/user.rb'
require './github/client.rb'
require './github/repo.rb'
require './github/organization.rb'

ENV['TOKEN'] = 'YOUR_TOKEN'

user = User.from_username 'kondzolko'

puts user.inspect
puts 'USER'

puts user.repos.inspect
puts 'REPOS'

puts user.followers[0].organizations.inspect
puts 'ORGANIZATIONS'

puts user.repos[0].assignees.inspect
puts 'ASSIGNESS'