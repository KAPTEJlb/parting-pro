def recursive_sum(int)
  int = int.to_s.chars.map(&:to_i)
  int.count == 1 ? int.first : recursive_sum(int.sum)
end

recursive_sum(16)
recursive_sum(942)
recursive_sum(132189)
recursive_sum(493193)